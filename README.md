# Interview
This README.md documents the steps to get started with the timed technical test (1 day). 

## Steps to run:
1. Install [Jupyter Notebook](https://jupyter.org/install)
2. Clone the repository
```bash
git clone https://gitlab.com/ianlimle1234/navdeck-ai-interview.git
cd navdeck-ai-interview
```
3. Open a shell using Jupyter Notebook
```bash 
jupyter notebook
```
4. For submission, zip the root folder and email the submission to `ianlim@navdeck.sg`